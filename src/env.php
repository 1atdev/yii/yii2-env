<?php
declare(strict_types=1);

use IsAtDev\Env\DotEnvLoader;

if (!function_exists('env')) {
    /**
     * Get a value from environment variable.
     *
     * @param string $name The environment variable name.
     * @param mixed $default The default value. Default is false.
     *
     * @return mixed
     */
    function env(string $name, mixed $default = null): mixed
    {
        return DotEnvLoader::get($name, $default);
    }
}

if (!function_exists('saveEnv')) {
    /**
     * Save a value to environment variable.
     *
     * @param string|array $key
     * @param string|array|int|float|bool $value
     * @param string|null $toFilePath
     *
     * @return void
     * @throws Exception
     */
	function saveEnv(string|array $key, string|array|int|float|bool $value, null|string $toFilePath = null): void
    {
        DotEnvLoader::set($key, $value, false, $toFilePath);
    }
}

if (!function_exists('replaceEnv')) {
    /**
     * Replace a value to environment variable.
     *
     * @param string|array $key
     * @param string|array|int|float|bool $value
     * @param string|null $toFilePath
     *
     * @return void
     * @throws Exception
     */
	function replaceEnv(string|array $key, string|array|int|float|bool $value, null|string $toFilePath = null): void
    {
        DotEnvLoader::set($key, $value, true, $toFilePath);
    }
}

if (!function_exists('value')) {
    /**
     * Return the default value of the given value.
     *
     * @param mixed $value
     * @param mixed ...$args
     * @return mixed
     */
    function value(mixed $value, ...$args): mixed
    {
        return $value instanceof Closure ? $value(...$args) : $value;
    }
}