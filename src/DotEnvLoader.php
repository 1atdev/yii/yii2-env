<?php
declare(strict_types=1);

namespace IsAtDev\Env;

use Dotenv\Dotenv;
use Exception;
use PhpOption\Option;
use Yii;
use yii\base\BaseObject;
use yii\helpers\Inflector;
use yii\helpers\Json;

final class DotEnvLoader extends BaseObject
{
	/**
	 * @var ?Dotenv
	 */
	protected static ?Dotenv $repository = null;
	
	/**
	 * @var bool
	 */
	protected static bool $isLoaded = false;
	
	public static function reset(): void
	{
		self::$isLoaded = false;
		self::$repository = null;
		
		$repository = self::getRepository();
		
		if ($repository) {
			$repository->safeLoad();
			
			self::$isLoaded = true;
		}
	}
	
	/**
	 * @return Dotenv|false
	 */
	protected static function getRepository(): Dotenv|false
	{
		if (self::$repository === null) {
			[$path, $file] = self::getEnvFile();
			
			if (empty($path) || empty($file)) {
				return false;
			}
			
			$overload = defined('DOTENV_OVERLOAD') ? DOTENV_OVERLOAD : false;
			
			/**
			 * Overload or load method by environment variable COMPOSER_DOTENV_OVERLOAD.
			 */
			$dotEnv = $overload ? Dotenv::createUnsafeImmutable($path, $file) : Dotenv::createUnsafeMutable($path, $file);
			
			self::$repository = $dotEnv;
		}
		
		return self::$repository;
	}
	
	protected static function getEnvFile(): array
	{
		$path = defined('DOTENV_PATH') ? DOTENV_PATH : '';
		$file = defined('DOTENV_FILE') ? DOTENV_FILE : '';
		
		/**
		 * Find Composer base directory.
		 */
		if ($path === '') {
			if (class_exists('Yii', false)) {
				/**
				 * Usually, the env is used before defining these aliases:
				 *
				 * @vendor and @app. So, if you vendor is symbolic link,
				 * Please register @vendor alias in bootstrap file or before
				 * call env function.
				 */
				if (Yii::getAlias('@vendor', false)) {
					$vendorDir = Yii::getAlias('@vendor');
					$path = dirname($vendorDir);
				} else if (Yii::getAlias('@app', false)) {
					$path = Yii::getAlias('@app');
				} else {
					$yiiDir = Yii::getAlias('@yii');
					$path = dirname($yiiDir, 3);
				}
			} else {
				if (defined('VENDOR_PATH')) {
					$vendorDir = VENDOR_PATH;
				} else {
					/**
					 * If not found Yii class, will use composer vendor directory
					 * structure finding.
					 *
					 * Notice: this method are not handled process symbolic link!
					 */
					$vendorDir = dirname(__FILE__, 4);
				}
				$path = dirname($vendorDir);
			}
		}
		
		/**
		 * Get env file name from environment variable,
		 * if COMPOSER_DOTENV_FILE have been set.
		 */
		if ($file === '') {
			$file = '.env';
		}
		
		/**
		 * This program will not force the file to be loaded,
		 * if the file does not exist then return.
		 */
		if (!file_exists(rtrim($path, DIRECTORY_SEPARATOR) . DIRECTORY_SEPARATOR . $file)) {
			return [];
		}
		
		return [$path, $file];
	}
	
	/**
	 * @param $key
	 * @param $default
	 *
	 * @return mixed
	 */
	public static function get($key, $default = null): mixed
	{
		$repository = self::getRepository();
		
		if (!$repository) {
			return $default;
		}
		
		if (!self::$isLoaded) {
			$repository->safeLoad();
			
			self::$isLoaded = true;
		}
		
		$getEnv = getenv(strtoupper($key));
		
		// its seem that `key` is not exists
		if (is_bool($getEnv) && $getEnv === false) {
			if (!is_null($default) && $default !== false) {
				return $default;
			}
		}
		
		return self::parseValue($getEnv, $default);
	}
	
	public static function parseValue($values, $default)
	{
		$returnedValues = Option::fromValue($values)
			->map(function ($value) {
				if (is_string($value)) {
					$match = match (strtolower($value)) {
						'true', '(true)' => true,
						'false', '(false)' => false,
						'empty', '(empty)' => '',
						'null', '(null)' => null,
						default => $value,
					};
					
					if (is_string($match)) {
						if (str_starts_with($match, 'json:')) {
							$match = str_replace('json:', '', $match);
							
							if (!empty($match)) {
								$match = Json::decode($match);
							} else {
								$match = [];
							}
							
							return $match;
						}
						
						if (str_starts_with($match, 'array:')) {
							$match = str_replace('array:', '', $match);
							
							if (!empty($match)) {
								$match = explode(',', $match);
							} else {
								$match = [];
							}
							
							return $match;
						}
						
						if (str_starts_with($match, 'int:')) {
							return (int)str_replace('int:', '', $match);
						}
						
						if (str_starts_with($match, 'float:')) {
							return (float)str_replace('float:', '', $match);
						}
					}
					
					return $match;
				}
				
				return $value;
			})
			->getOrCall(function () use ($default) {
				return value($default);
			});
		
		if (!is_null($default)) {
			if ($default !== $returnedValues) {
				if ($returnedValues === false) {
					return $default;
				}
				
				if (is_null($returnedValues)) {
					return $default;
				}
			}
		}
		
		return $returnedValues;
	}
	
	public static function put($key, $value): void
	{
		putenv(strtoupper($key) . "=" . $value);
	}
	
	/**
	 * @param string|array                $key
	 * @param string|array|int|float|bool $value
	 * @param bool                        $replace
	 * @param string|null                 $toFilePath
	 *
	 * @return void
	 * @throws Exception
	 */
	public static function set(string|array $key, string|array|int|float|bool $value, bool $replace = false, string|null $toFilePath = null): void
	{
		if (empty($toFilePath)) {
			[$path, $file] = self::getEnvFile();
			
			if (empty($path) || empty($file)) {
				throw new Exception('File .env not found!');
			}
			
			$toFilePath = "$path/$file";
		}
		
		if (!file_exists($toFilePath)) {
			throw new Exception("File '$toFilePath' not found!");
		}
		
		if (!is_writeable($toFilePath)) {
			throw new Exception("File '$toFilePath' not writeable!");
		}
		
		$content = file_get_contents("$toFilePath");
		
		if (is_array($key)) {
			foreach ($key as $idx => $item) {
				$setVal = $value[$idx];
				
				if (is_bool($setVal)) {
					$setVal = $setVal === true ? 'true' : 'false';
				}
				
				$content = self::loopSet($content, $item, $setVal, $replace);
			}
		} else {
			if (is_bool($value)) {
				$value = $value === true ? 'true' : 'false';
			}
			
			$content = self::loopSet($content, $key, $value, $replace);
		}
		
		$content = preg_replace("/[\n]{3,}/m", "\n\n", $content);
		
		file_put_contents("$toFilePath", $content);
	}
	
	public static function loopSet(string $content, string $key, string|array|int|float $value, bool $replace = false): array|string
	{
		// mb_strtolower(preg_replace('/(?<=\\pL)(\\p{Lu})/u', '_\\1', $words), self::encoding())
		// $key = strtoupper(Inflector::underscore(strtolower($key)));
		$key = strtoupper(mb_strtolower(preg_replace('/(?<=\\pL)(\\p{Lu})/u', '_\\1', strtolower($key)), 'UTF-8'));
		$value = self::toValue($value);
		
		if (preg_match("/^$key=/mi", $content)) {
			$replace = true;
		}
		
		if ($replace) {
			$content = preg_replace("/^$key=?(\'|\")?(.*)(\'|\")?$/mi", "$key={{NEW_VALUE}}", $content);
		} else {
			$content .= "$key={{NEW_VALUE}}\n";
		}
		
		if (in_array($value, ['true', 'false'])) {
			$content = str_replace("{{NEW_VALUE}}", $value, $content);
		} else {
			$content = str_replace('{{NEW_VALUE}}', "'$value'", $content);
		}
		
		return $content;
	}
	
	protected static function toValue($value)
	{
		if (is_array($value)) {
			$value = 'array:' . implode(',', $value);
		}
		
		if (is_int($value)) {
			$value = 'int:' . $value;
		}
		
		if (is_float($value)) {
			$value = 'float:' . $value;
		}
		
		if (is_bool($value)) {
			$value = $value ? 'true' : 'false';
		}
		
		if (!in_array($value, ['true', 'false'])) {
			if (str_starts_with($value, '"')) {
				$value = trim($value, '"');
			}
			
			if (str_starts_with($value, "'")) {
				$value = trim($value, "'");
			}
			
			// if (str_starts_with($value, 'json:')) {
			// 	$value = "'$value'";
			// } else {
			// 	$value = "\"$value\"";
			// }
		}
		
		return $value;
	}
}
